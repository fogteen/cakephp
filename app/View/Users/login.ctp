<div class="users form">
    <?php echo $this->Flash->render('auth'); ?>
    <?php echo $this->Form->create('User'); ?>
    <fieldset>
        <legend>
            <?php echo __('Nhập tên tài khoản và mật khẩu'); ?>
        </legend>
        <?php echo $this->Form->input('email');
        echo $this->Form->input('password');
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Login')); ?>
</div>

<fieldset>
    <legend>
        <?php echo __('Alternatively, you can :');?>
    </legend>
</fieldset>
<a class="btn-auth btn-facebook large" href=<?php echo 'fblogin'; ?>> Log in with <b>Facebook</b></a>