<div class="users form">
	<?php echo $this->Form->create('User', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Edit User'); ?></legend>
		<?php echo $this->Form->input('email');
		echo $this->Form->input('password');
		echo $this->Form->input('role', array(
			'options' => array('admin' => 'Admin', 'author' => 'Author')
		));
		echo $this->Html->image('../files/user/picture/' . $user['User']['piture_dir'] . '/thumb_' . $user['User']['picture']);
		echo $this->Form->input('User.picture.remove', array('type' => 'checkbox', 'label' => 'Remove existing file'));
		echo $this->Form->input('picture', array('type' => 'file'));
		echo $this->Form->input('piture_dir', array('type' => 'hidden'));

		?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>