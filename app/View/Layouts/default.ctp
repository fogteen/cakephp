<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
	<?= $this->Html->charset() ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>
		<?= $cakeDescription ?>:
		<?= $this->fetch('title') ?>
	</title>
	<?= $this->Html->meta('icon') ?>

	<?= $this->Html->css('base.css') ?>
	<?= $this->Html->css('cake.css') ?>

	<?= $this->fetch('meta') ?>
	<?= $this->fetch('css') ?>
	<?= $this->fetch('script') ?>
</head>
<body>
<?= $this->Flash->render() ?>
<section class="container clearfix">
	<?php if ($this->Session->check('user')){ ?>
		<p>Xin chào: <?= $this->Session->read('user') ?></p>
		<p><?= $this->Html->link(__('Log out'),array('controller' => 'users', 'action' => 'logout')) ?></p>
	<?php } ?>
	<?= $this->fetch('content') ?>
</section>
<footer>
</footer>
<?php echo $this->Js->writeBuffer(); // By default scripts are cached, and we must explicitly print out the cache ?>
<?= $this->Html->script('vendor/jquery.min') ?>
<script>
	$(document).ready(function() {
		$('#email').on('blur', function (e) {

			$.ajax({
				url: '/cakephp/users/check',
				type: 'POST',
				dataType: 'json',
				data: {'mail': $('#email').val()},
				success: function (data, status) {
					console.log(data.text);
					if (data.text == "ok") {
						$('#result').html('<p><em>OK!</em></p>');
					}
				},
				error: function (xhr, desc, err) {
					console.log(xhr);
					console.log("Details: " + desc + "\nError:" + err);
				}
			}); // end ajax call
		});
	});
</script>
</body>
</html>
