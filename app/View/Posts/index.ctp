<h1>All Posts</h1>
<?= $this->Html->link(__("New Post"), array('action' => 'add')) ?>
<table>
    <tr>
        <th>Id</th>
        <th>Title</th>
        <th>Created</th>
        <th>Action</th>
    </tr>
    <?php foreach($posts as $post): ?>
    <tr>
        <td><?= $post['Post']['id'] ?></td>
        <td><?= $this->Html->link($post['Post']['title'], array('action' => 'view',$post['Post']['id'])) ?></td>
        <td><?= $this->Time->format($post['Post']['created'], "%d-%m-%y %h:%m:%s") ?></td>
        <td>
            <?= $this->Html->link(__("Edit"), array('action' => 'edit',$post['Post']['id'])) ?>
            <?= $this->Form->postLink(__("Delete"), array('action' => 'delete',$post['Post']['id']),array('confirm' => 'Xóa post?')) ?>
        </td>
    </tr>
    <?php endforeach ?>
</table>
<p class="paging">
<?php
echo $this->Paginator->prev('« Truoc ', null, null, array('class' => 'disabled')); //nút Previous
echo $this->Paginator->numbers(); //Hiển thi các số phân trang
echo $this->Paginator->next(' Sau »', null, null, array('class' => 'disabled')); //Hiển thị nút next
echo " Page ".$this->Paginator->counter(); // Hiển thị tổng trang
?>
</p>
