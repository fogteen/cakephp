<?php
/**
 * Created by PhpStorm.
 * Post: hoang
 * Date: 02/03/2016
 * Time: 15:57
 */

class PostsController extends AppController {

    public $helpers = array('Html', 'Form', 'Paginator');

    public function index() {
        $this->paginate = array('limit' => 5); //Phân trang với 5 item
        $this->set('posts', $this->paginate('Post'));
    }

    public function view($id = null) {
        if (empty($this->Post->findById($id))) {
            $this->Flash->error(__("Không tìm thấy dữ liệu"));
            return $this->redirect(array('action' => 'index'));
        } else {
            $this->set('post', $this->Post->findById($id));
        }
    }

    public function add() {
        if ($this->request->is('post')) {
            $this->Post->create();
            if ($this->Post->save($this->request->data)) {
                $this->Flash->success(__('Tạo thành công.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Xảy ra lỗi.'));
        }
    }

    public function edit($id = null) {
        if (empty($this->Post->findById($id))) {
            $this->Flash->error(__("Không tìm thấy dữ liệu"));
            return $this->redirect(array('action' => 'index'));
        }
        elseif ($this->request->is(array('post', 'put'))) {
            $this->Post->id = $id;
            if ($this->Post->save($this->request->data)) {
                $this->Flash->success(__('Cập nhật thành công.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Đã xảy ra lỗi'));
        }

        if (!$this->request->data) {
            $post = $this->Post->findById($id);
            $this->request->data = $post;
        }
    }

    public function delete($id) {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }

        if ($this->Post->delete($id)) {
            $this->Flash->success(
                __('Đã xóa post có id: %s', h($id))
            );
        } else {
            $this->Flash->error(
                __('Không thể xóa post có id: %s', h($id))
            );
        }

        return $this->redirect(array('action' => 'index'));
    }

    public function isAuthorized($Post) {
        // All registered Posts can add posts
        if ($this->action === 'add') {
            return true;
        }

        // The owner of a post can edit and delete it
        if (in_array($this->action, array('edit', 'delete'))) {
            $postId = (int) $this->request->params['pass'][0];
            if ($this->Post->isOwnedBy($postId, $Post['id'])) {
                return true;
            }
        }

        return parent::isAuthorized($Post);
    }
}